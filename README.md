# Partysongs

## Setup fresh database

    export DATABASE_URL="postgres://postgres:password@localhost/partysongs_dev"
    diesel database reset
    cargo run --bin load_fixtures

## Run Rocket dev server
    watchexec --exts rs,toml --restart "cargo run --bin partysongs"
