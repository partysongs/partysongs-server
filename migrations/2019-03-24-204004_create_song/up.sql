-- Your SQL goes here
CREATE TABLE song (
  id SERIAL PRIMARY KEY,
  artist VARCHAR NOT NULL,
  title VARCHAR NOT NULL,
  content TEXT NOT NULL
)
