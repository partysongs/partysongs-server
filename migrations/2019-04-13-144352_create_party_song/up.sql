-- Your SQL goes here

CREATE TYPE party_song_status_type AS ENUM
   ('CURRENT',
    'PLAYED',
    'TOPLAY');

CREATE TABLE party_song (
  party INTEGER REFERENCES party ON DELETE CASCADE,
  song INTEGER REFERENCES song ON DELETE CASCADE,
  status party_song_status_type NOT NULL DEFAULT 'TOPLAY',
  PRIMARY KEY (party, song)
);
