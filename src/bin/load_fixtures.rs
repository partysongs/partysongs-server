extern crate partysongs;
use partysongs::establish_connection;
use partysongs::db::song::NewFixtureSong;
use partysongs::db::party::NewFixtureParty;
use partysongs::db;
use partysongs::models::song::Song;
use partysongs::models::party::Party;
use diesel::result::Error;
use log::{error, info};
use simplelog::{CombinedLogger, Config, LevelFilter,TermLogger, WriteLogger};
use std::fs::File;

fn main() {
    CombinedLogger::init(
        vec![
            TermLogger::new(LevelFilter::Info, Config::default()).unwrap(),
            WriteLogger::new(LevelFilter::Error, Config::default(), File::create("partysongs.log").unwrap()),
        ]
    ).unwrap();

    let connection = establish_connection();

    let new_parties =
        vec!(
            NewFixtureParty{id:1, title: String::from("Test party 1")},
            NewFixtureParty{id:2, title: String::from("Another party 2")}
        );


    let new_songs =
        vec!(
            NewFixtureSong{
                id:1,
                artist: String::from("Téléphone"),
                title: String::from("New York avec toi"),
                content: String::from("A                   F#m
 Un jour j'irai à New-York avec toi 
 A                      B 
 Toutes les nuits déconner 
 A                       F#m 
 Et voir aucun film en entier, ça va d'soi 
 A                  B          D 
 Avoir la vie partagée, tailladée 
    A                           F#m 
 Bercés par le ronron de l'air conditionné 
    A                       B 
 Dormir dans un hôtel délatté 
    A                                F#m 
 Traîner du coté gay et voir leurs corps se serrer 
       A                         B 
 Voir leurs coeurs se vider et saigner 
          D 
 Oui, saigner 

 A     B            D 
 Un jour j'irai là-bas 
 A        B              D 
 Un jour chat, un autre rat 
 A                       B            D 
 Voir si le coeur de la ville bat en toi 
               A 
 Et tu m'emmèneras 

 A    F#m 

         A 
 Emmène moi


 A    F#m   (3 fois) 

 Un jour j'aurai New-York au bout des doigts 
 On y jouera, tu verras 
 Dans les clubs il fait noir, mais il ne fait pas froid 
 Il ne fait pas froid si t'y crois 
 Et j'y crois
 Les flaques de peinture sur les murs ont parfois 
 La couleur des sons que tu bois 
 Et puis c'est tellement grand que vite on oubliera 
 Que nulle part c'est chez moi
 Chez toi, chez nous quoi

 Refrain

 Emmène-moi, emmène-moi
 Emmène-moi, emmène-moi
 Toucher à ci, toucher à ça 
 Voir si le coeur de la ville bat en moi 
 Et tu m'emmèneras

 A    F#m 

          A 
 Emmène moi

 A    E    A")},
            NewFixtureSong {
                id: 2,
                artist: String::from("Oasis"),
                title: String::from("Don't look back in anger"),
                content: String::from("[Intro]

|C    |F    |C    |F    |

[Verse 1]
C               G            Am
Slip inside the eye of your mind
          E              F    G
Don't you know you might find
                  C      Am G
A better place to play
C             G           Am
You said that you'd never been
            E                  F    G
But all the things that you've seen
             C    Am G F
Slowly fade away

[Pre-Chorus]
F                Fm             C
So I start a revolution from my bed
         F                 Fm             C
'Cos you said the brains I had went to my head
F                 Fm              C
Step outside, the summertime's in bloom
G
Stand up beside the fireplace
    E7/G#
And take that look from off your face
Am             G             F         G
You ain't ever gonna burn my heart out

[Chorus]
C  G         Am        E              F
So Sally can wait, she knows it's too late
         G          C   Am G
as we're walking on by
    C    G       Am   E
Her soul slides away,
               F             G
but don't look back in anger
            C   G   Am   E   F   G   C   Am G
I heard you say

[Verse 2]
C              G               Am
Take me to the place where you go
      E      F      G                C    Am G
Where nobody knows, if it's night or day
C                     G           Am
Please don't put your life in the hands
     E             F     G                    C    Am G
of a rock and roll band, who'll throw it all away

[Pre-Chorus]
F                     Fm             C
I'm gonna start a revolution from my bed
         F                 Fm             C
'Cos you said the brains I had went to my head
F                 Fm              C
Step outside, the summertime's in bloom
G
Stand up beside the fireplace
E7/G#
Take that look from off your face
     Am             G             F         G
'Cos you ain't ever gonna burn my heart out

[Chorus]
C  G         Am        E              F
So Sally can wait, she knows it's too late
         G          C   Am G
as she's walking on by
   C    G       Am   E
My soul slides away,
               F             G
but don't look back in anger
            C   Am G
I heard you say

[Solo]
|F  Fm  |C     | x3
|G      |E/G#  |
|Am  G  |F     |G     |G N.C. |

[Chorus]
C  G         Am        E              F
So Sally can wait, she knows it's too late
         G          C   Am G
as we're walking on by
    C    G       Am   E
Her soul slides away,
               F             G
but don't look back in anger
            C    Am G
I heard you say
C  G         Am        E              F
So Sally can wait, she knows it's too late
         G          C   Am G
as she's walking on by
   C    G       Am
My soul slides away
               F
But don't look back in anger
           Fm
Don't look back in anger
            C   G   Am   E   F   Fm               C
I heard you say                    At least not today
")},
        NewFixtureSong {
            id: 3,
            artist: String::from("Tuesday"),
            title: String::from("Startup Nation"),
            content: String::from("Some content")
        }
    );

   let songs: Result<Vec<Song>, Error> =
       db::song::batch_insert_fixture(&connection, &new_songs);
   let parties: Result<Vec<Party>, Error> =
       db::party::batch_insert_fixture(&connection, &new_parties);

   match (songs, parties) {
        (Ok(s), Ok(p)) => info!("{} songs and {} parties imported", s.len(), p.len()),
        (Err(e), Ok(_)) => info!("Error importing songs {}", e),
        (Ok(_), Err(e)) => info!("Error importing parties {}", e),
        (Err(es), Err(ep)) => error!("Error importing fixtures songs and parties {} {}", es, ep),
   };

    db::song::attach_to_party(&connection, 1, 1, None).unwrap();
    db::song::attach_to_party(&connection, 2, 1, Some(crate::db::PartySongStatusEnum::Current)).unwrap();
    db::song::attach_to_party(&connection, 3, 2, None).unwrap();
    db::song::attach_to_party(&connection, 1, 2, None).unwrap();


}
