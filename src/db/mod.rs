pub mod party;
pub mod song;

use diesel::deserialize::{self, FromSql};
use diesel::pg::Pg;
use diesel::serialize::{self, IsNull, Output, ToSql};
use std::io::Write;

#[derive(SqlType)]
#[postgres(type_name = "party_song_status_type")]
pub struct PartySongStatusType;

#[derive(Debug, PartialEq, FromSqlRow, AsExpression)]
#[sql_type = "PartySongStatusType"]
pub enum PartySongStatusEnum {
    Current,
    Played,
    ToPlay,
}

impl ToSql<PartySongStatusType, Pg> for PartySongStatusEnum {
    fn to_sql<W: Write>(&self, out: &mut Output<W, Pg>) -> serialize::Result {
        match *self {
            PartySongStatusEnum::Current => out.write_all(b"CURRENT")?,
            PartySongStatusEnum::Played => out.write_all(b"PLAYED")?,
            PartySongStatusEnum::ToPlay => out.write_all(b"TOPLAY")?,
        }
        Ok(IsNull::No)
    }
}

impl FromSql<PartySongStatusType, Pg> for PartySongStatusEnum {
    fn from_sql(bytes: Option<&[u8]>) -> deserialize::Result<Self> {
        match not_none!(bytes) {
            b"CURRENT" => Ok(PartySongStatusEnum::Current),
            b"PLAYED" => Ok(PartySongStatusEnum::Played),
            b"TOPLAY" => Ok(PartySongStatusEnum::ToPlay),
            _ => Err("Unrecognized enum variant".into()),
        }
    }
}
