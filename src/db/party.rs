use crate::schema::party;
use crate::models::party::Party;
use diesel::PgConnection;
use diesel::{self, prelude::*};
use crate::schema::party::dsl::{party as all_parties};

#[table_name="party"]
#[derive(Serialize, Insertable, Debug, Clone)]
pub struct NewParty {
    pub title: String
}

#[table_name="party"]
#[derive(Serialize, Insertable, Debug, Clone)]
pub struct NewFixtureParty {
    pub id: i32,
    pub title: String
}

pub fn insert(conn: &PgConnection, party: &NewParty) -> Result<Party, diesel::result::Error> {
    diesel::insert_into(party::table).values(party).get_result(&*conn)
}


pub fn batch_insert(conn: &PgConnection, party: &Vec<NewParty>) -> Result<Vec<Party>, diesel::result::Error> {
    diesel::insert_into(party::table).values(party).get_results(&*conn)
}


pub fn batch_insert_fixture(conn: &PgConnection, party: &Vec<NewFixtureParty>) -> Result<Vec<Party>, diesel::result::Error> {
    diesel::insert_into(party::table).values(party).get_results(&*conn)
}

pub fn all(conn: &PgConnection) -> Vec<Party> {
    all_parties.order(party::id.desc()).load::<Party>(conn).unwrap()
}
