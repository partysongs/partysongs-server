use diesel::PgConnection;
use diesel::result::Error;
use diesel::{self, prelude::*};
use crate::schema::{party_song, song};
use crate::models::song::Song;


const DEFAULT_LIMIT: i64 = 20;

#[table_name="song"]
#[derive(Serialize, Insertable, Debug, Clone)]
pub struct NewSong {
    pub artist: String,
    pub title: String,
    pub content: String
}


#[table_name="song"]
#[derive(Serialize, Insertable, Debug, Clone)]
pub struct NewFixtureSong {
    pub id: i32,
    pub artist: String,
    pub title: String,
    pub content: String
}


#[derive(FromForm, Default)]
pub struct FindSongs {
    limit: Option<i64>,
    offset: Option<i64>,
}


pub fn insert(conn: &PgConnection, song: &NewSong) -> Result<Song, Error> {
    diesel::insert_into(song::table).values(song).get_result(&*conn)
}

pub fn batch_insert(conn: &PgConnection, song: &Vec<NewSong>) -> Result<Vec<Song>, Error> {
    diesel::insert_into(song::table).values(song).get_results(&*conn)
}


pub fn batch_insert_fixture(conn: &PgConnection, song: &Vec<NewFixtureSong>) -> Result<Vec<Song>, Error> {
    diesel::insert_into(song::table).values(song).get_results(&*conn)
}

pub fn find(conn: &PgConnection, params: &FindSongs, party_id: Option<i32>) -> Vec<Song> {

    let query = song::table
        .left_join(
            party_song::table.on(song::id
                .eq(party_song::song)
                .and(party_song::party.eq(party_id.unwrap_or(0)))), // TODO: refactor
        )
        .select(
            song::all_columns,
        )
        .into_boxed();

    query
        .limit(params.limit.unwrap_or(DEFAULT_LIMIT))
        .offset(params.offset.unwrap_or(0))
        .load::<Song>(conn)
        .expect("Cannot load songs")
}


pub fn attach_to_party(conn: &PgConnection, song_id: i32, party_id: i32, status: Option<crate::db::PartySongStatusEnum>) -> Result<usize, Error> {

        diesel::insert_into(party_song::table)
            .values((
                party_song::party.eq(party_id),
                party_song::song.eq(song_id),
                party_song::status.eq(status.unwrap_or(crate::db::PartySongStatusEnum::ToPlay)),
            ))
            .execute(conn)
}
