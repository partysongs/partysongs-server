#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;

use partysongs::models::song::Song;
use partysongs::db;

use rocket_contrib::json::{Json, JsonValue};
use rocket::request::Form;
use diesel::PgConnection;
use ws::listen;
use std::thread;

#[database("partysongs")]
pub struct DbConn(PgConnection);

#[get("/?<params..>", format = "json")]
pub fn index(
    params: Form<db::song::FindSongs>,
    conn: DbConn) -> Json<Vec<Song>> {
        Json(db::song::find(&conn, &params, None))
}

#[catch(404)]
fn not_found() -> JsonValue {
    json!({
        "status": "error",
        "reason": "Resource was not found."
    })
}


fn main() {

    thread::spawn(move || {
        // Listen on an address and call the closure for each connection
        if let Err(error) = listen("127.0.0.1:3012", |out| {

            println!("New connexion.");

            // The handler needs to take ownership of out, so we use move
            move |msg| {

                // Handle messages received on this connection
                println!("Server got message '{}'. ", msg);

                // Use the out channel to send messages back
                out.send(msg)
            }

        }) {
            // Inform the user of failure
            println!("Failed to create WebSocket due to {:?}", error);
        }

    });


    rocket::ignite()
        .attach(DbConn::fairing())
        .register(catchers![not_found])
        .mount("/", routes![index]).launch();
}
