use chrono::{DateTime, Utc};

#[derive(Queryable, Debug, Clone)]
pub struct Party {
    pub id: i32,
    pub title: String,
    pub created_at: DateTime<Utc>
}
