#[derive(Serialize, Queryable, Debug, Clone)]
pub struct Song {
    pub id: i32,
    pub artist: String,
    pub title: String,
    pub content: String
}
