table! {
    party (id) {
        id -> Int4,
        title -> Varchar,
        created_at -> Timestamptz,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::db::PartySongStatusType;

    party_song (party, song) {
        party -> Int4,
        song -> Int4,
        status -> PartySongStatusType,
    }
}

table! {
    song (id) {
        id -> Int4,
        artist -> Varchar,
        title -> Varchar,
        content -> Text,
    }
}

joinable!(party_song -> party (party));
joinable!(party_song -> song (song));

allow_tables_to_appear_in_same_query!(
    party,
    party_song,
    song,
);
